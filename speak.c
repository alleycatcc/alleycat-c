#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "include/private/colors.h"
#include "include/private/mem.h"
#include "include/private/speak.h"
#include "include/private/squeak.h"
#include "include/private/squeak-macros.h"
#include "include/private/util.h"

static const int INFO_LENGTH = 200;

void ac_info (const char * const format, ...) {
  char new[INFO_LENGTH];
  va_list arglist;
  va_start (arglist, format);
  int const rc = vsnprintf (new, INFO_LENGTH, format, arglist);
  if (rc >= INFO_LENGTH) ac_warn ("ac_info (): string truncated.");
  va_end (arglist);

  char *bullet = ac_m_bright_blue (get_bullet ());
  printf ("%s %s\n", bullet, new);
  free (bullet);
}

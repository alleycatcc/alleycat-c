#ifndef _AC_STRING_H
#define _AC_STRING_H

extern char * const ac_s, * const ac_t, * const ac_u, * const ac_v,
            * const ac_w, * const ac_x, * const ac_y, * const ac_z;
void ac_ ();

void ac_spr (const char *format, ...);

void ac_static_strings_save ();
void ac_static_strings_restore ();

#endif

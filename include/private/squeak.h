#ifndef _AC_SQUEAK_H
#define _AC_SQUEAK_H

#include <stdbool.h>

void ac_complain (const char *file, unsigned int line, const char *func, bool iserr, bool do_perr, const char *format, ...);
char *ac_perr ();

#endif

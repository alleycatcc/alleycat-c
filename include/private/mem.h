#ifndef _AC_MEM_H
#define _AC_MEM_H

#include <stddef.h>

void *ac_malloc (size_t s);
void *ac_malloc_non_fatal (size_t s);
void *ac_calloc (size_t nmemb, size_t size);
void *ac_calloc_non_fatal (size_t nmemb, size_t size);
void *ac_realloc (void *ptr, size_t size);
void *ac_realloc_non_fatal (void *ptr, size_t size);

#endif

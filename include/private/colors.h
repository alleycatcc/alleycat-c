#ifndef _AC_COLORS_H
#define _AC_COLORS_H

// --- length of longest color escape.
static const int COLOR_LENGTH = 5;
static const int COLOR_RESET_LENGTH = 4;

char *ac_m_bright_blue (const char *s);
char *ac_m_red (const char *s);
char *ac_m_bright_red (const char *s);
char *ac_m_green (const char *s);
char *ac_m_bright_green (const char *s);
char *ac_m_yellow (const char *s);
char *ac_m_bright_yellow (const char *s);
char *ac_m_blue (const char *s);
char *ac_m_bright_blue (const char *s);
char *ac_m_cyan (const char *s);
char *ac_m_bright_cyan (const char *s);
char *ac_m_magenta (const char *s);
char *ac_m_bright_magenta (const char *s);

void ac_red (const char *s);
void ac_bright_red (const char *s);
void ac_green (const char *s);
void ac_bright_green (const char *s);
void ac_yellow (const char *s);
void ac_bright_yellow (const char *s);
void ac_blue (const char *s);
void ac_bright_blue (const char *s);
void ac_cyan (const char *s);
void ac_bright_cyan (const char *s);
void ac_magenta (const char *s);
void ac_bright_magenta (const char *s);

void ac_disable_colors ();
void ac_enable_colors ();
void ac_force_colors ();
void ac_unforce_colors ();

#endif

#ifndef _AC_UTIL_H
#define _AC_UTIL_H

#include <stdio.h>

const char *get_bullet ();
void ac_autoflush (FILE *handle);

bool ac_atod (const char *s, double *ret);
bool ac_atoi (const char *s, int *ret);
int ac_int_length (long i);

#endif

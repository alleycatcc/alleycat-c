/** Public (re-exported by alleycat.h)
 */

#ifndef _AC_MEM_MACROS_H
#define _AC_MEM_MACROS_H

#include "mem.h"

/* Usage:
 *
 * On error, the _rf variants return false, the _rnull variants return null and the _die variants
 * cause the program to exit. Not all variants are shown in the usage.
 *
 * Some of the macros here are wrappd in do/while (0) and are therefore safe
 * to use in an if/else without braces. Some declare variables, meaning they
 * can not be wrapped in do/while and are therefore not safe in an if/else
 * without braces, but it's also not clear how useful it would be to declare
 * a variable in such a case.
 *
 *   ac_try_malloc (struct state *, the_state)
 *   // struct state *the_state = malloc (sizeof (*the_state);
 *   if (the_state == NULL) ...
 *
 *   ac_try_malloc (struct state **, the_state)
 *   // struct state **the_state = malloc (sizeof (*the_state);
 *   if (the_state == NULL) ...
 *
 *   ac_try_malloc_rf (struct state, the_state)
 *   // struct state *the_state = malloc (sizeof (*the_state);
 *   // if (the_state == NULL) return false;
 *
 *   struct state *the_state;
 *   ac_try_malloc_size_nodecl_rf (the_state, some_size)
 *   // the_state = malloc (some_size)
 *
 *   ac_try_malloc_size (struct state, the_state, some_size)
 *   // struct state *the_state = malloc (some_size);
 *   if (the_state == NULL) ...
 *
 *   ac_try_malloc_size (struct state *, the_state, some_size)
 *   // struct state **the_state = malloc (some_size);
 *   if (the_state == NULL) ...
 *
 *   ac_try_malloc_size (const struct state, the_state, some_size)
 *   // const struct state *the_state = malloc (some_size);
 *   if (the_state == NULL) ...
 *
 *   ac_try_malloc_size (const struct state, const the_state, some_size)
 *   // const struct state * const the_state = malloc (some_size);
 *   if (the_state == NULL) ...
 *
 *   ac_try_xxx_die (...)
 *   // message and exit (1) on failure
 *
 *   ac_try_calloc_rf (struct state **, the_state, 10)
 *   // struct state **the_state = calloc (10, sizeof (*the_state))
 *
 *   struct state *the_state;
 *   ac_try_realloc_rf (struct state *, the_state_new, the_state)
 *   // struct state *the_state_new = realloc (the_state, sizeof (*the_state_new));
 *
 *   struct state *the_state;
 *   struct state *the_state_new;
 *   ac_try_realloc_size_nodecl_rf (the_state_new, the_state, some_size)
 *   // the_state_new = realloc (the_state, some_size);
 */

#define ac_try_malloc_size(type, var, size) \
  type var = ac_malloc_non_fatal (size);

#define ac_try_malloc_size_rnull(type, var, size) \
  type var = ac_malloc_non_fatal (size); \
  if (var == NULL) return NULL;

#define ac_try_malloc_size_rf(type, var, size) \
  type var = ac_malloc_non_fatal (size); \
  if (var == NULL) return false;

#define ac_try_malloc_size_die(type, var, size) \
  type var = ac_malloc (size);

#define ac_try_malloc(type, var) \
  ac_try_malloc_size (type, var, sizeof(*var));

#define ac_try_malloc_rnull(type, var) \
  ac_try_malloc_size_rnull (type, var, sizeof(*var));

#define ac_try_malloc_rf(type, var) \
  ac_try_malloc_size_rf (type, var, sizeof(*var));

#define ac_try_malloc_die(type, var) \
  ac_try_malloc_size_die (type, var, sizeof(*var));

#define ac_try_malloc_size_nodecl(var, size) do { \
  var = ac_malloc_non_fatal (size); \
} while (0)

#define ac_try_malloc_size_nodecl_rnull(var, size) do { \
  var = ac_malloc_non_fatal (size); \
  if (var == NULL) return NULL; \
} while (0)

#define ac_try_malloc_size_nodecl_rf(var, size) do { \
  var = ac_malloc_non_fatal (size); \
  if (var == NULL) return false; \
} while (0)

#define ac_try_malloc_size_nodecl_die(var, size) do { \
  var = ac_malloc (size); \
} while (0)


#define ac_try_calloc_size(type, var, nmemb, size) \
  type var = ac_calloc_non_fatal (nmemb, size);

#define ac_try_calloc_size_rnull(type, var, nmemb, size) \
  type var = ac_calloc_non_fatal (nmemb, size); \
  if (var == NULL) return NULL;

#define ac_try_calloc_size_rf(type, var, nmemb, size) \
  type var = ac_calloc_non_fatal (nmemb, size); \
  if (var == NULL) return false;

#define ac_try_calloc_size_die(type, var, nmemb, size) \
  type var = ac_calloc (nmemb, size);

#define ac_try_calloc(type, var, nmemb) \
  ac_try_calloc_size (type, var, nmemb, sizeof(*var));

#define ac_try_calloc_rnull(type, var, nmemb) \
  ac_try_calloc_size_rnull (type, var, nmemb, sizeof(*var));

#define ac_try_calloc_rf(type, var, nmemb) \
  ac_try_calloc_size_rf (type, var, nmemb, sizeof(*var));

#define ac_try_calloc_die(type, var, nmemb) \
  ac_try_calloc_size_die (type, var, nmemb, sizeof(*var));

#define ac_try_calloc_size_nodecl(var, nmemb, size) do { \
  var = ac_calloc_non_fatal (nmemb, size); \
} while (0)

#define ac_try_calloc_size_nodecl_rnull(var, nmemb, size) do { \
  var = ac_calloc_non_fatal (nmemb, size); \
  if (var == NULL) return NULL; \
} while (0)

#define ac_try_calloc_size_nodecl_rf(var, nmemb, size) do { \
  var = ac_calloc_non_fatal (nmemb, size); \
  if (var == NULL) return false; \
} while (0)

#define ac_try_calloc_size_nodecl_die(var, nmemb, size) do { \
  var = ac_calloc (nmemb, size); \
} while (0)


#define ac_try_realloc_size(type, var, ptr, size) \
  type var = ac_realloc_non_fatal (ptr, size);

#define ac_try_realloc_size_rnull(type, var, ptr, size) \
  type var = ac_realloc_non_fatal (ptr, size); \
  if (var == NULL) return NULL;

#define ac_try_realloc_size_rf(type, var, ptr, size) \
  type var = ac_realloc_non_fatal (ptr, size); \
  if (var == NULL) return false;

#define ac_try_realloc_size_die(type, var, ptr, size) \
  type var = ac_realloc (ptr, size);

#define ac_try_realloc(type, var, ptr) \
  ac_try_realloc_size (type, var, ptr, sizeof(*var));

#define ac_try_realloc_rnull(type, var, ptr) \
  ac_try_realloc_size_rnull (type, var, ptr, sizeof(*var));

#define ac_try_realloc_rf(type, var, ptr) \
  ac_try_realloc_size_rf (type, var, ptr, sizeof(*var));

#define ac_try_realloc_die(type, var, ptr) \
  ac_try_realloc_size_die (type, var, ptr, sizeof(*var));

#define ac_try_realloc_size_nodecl(var, ptr, size) do { \
  var = ac_realloc_non_fatal (ptr, size); \
} while (0)

#define ac_try_realloc_size_nodecl_rnull(var, ptr, size) do { \
  var = ac_realloc_non_fatal (ptr, size); \
  if (var == NULL) return NULL; \
} while (0)

#define ac_try_realloc_size_nodecl_rf(var, ptr, size) do { \
  var = ac_realloc_non_fatal (ptr, size); \
  if (var == NULL) return false; \
} while (0)

#define ac_try_realloc_size_nodecl_die(var, ptr, size) do { \
  var = ac_realloc (ptr, size); \
} while (0)

#endif

/** Public (re-exported by alleycat.h)
 */

#ifndef _AC_SQUEAK_MACROS_H
#define _AC_SQUEAK_MACROS_H

#include <stdlib.h>

/*
 * iwarn, ierr: intended for programmer (not user) errors.
 *
 * ac_die(...) and ac_idie(...) exit with status of 1.
 *
 * ac_iwarn("")/ac_piep:    <file>:<line> Something's wrong (internally).
 * ac_iwarn(msg):      <file>:<line> Internal warning: <msg>
 * ac_ierr("")/ac_die:      <file>:<line> Internal error.
 * ac_ierr(msg):       <file>:<line> Internal error: <msg>
 *
 * ac_iwarn_perr(""):    <file>:<line> Something's wrong (internally) (<system error>).
 * ac_iwarn_perr(msg): <file>:<line> Internal warning: <msg> (<system error>).
 * ac_ierr_perr(""):     <file>:<line> Internal error (<system error>).
 * ac_ierr_perr(msg):  <file>:<line> Internal error: <msg> (<system error>).
 *
 * ac_err, ac_warn: intended for user errors and system errors.
 *
 * ac_warn(""):          Something's wrong.
 * ac_warn(msg):       Warning: <msg>
 * ac_err(""):           Error.
 * ac_err(msg):        Error: <msg>
 *
 * ac_warn_perr(""):     Something's wrong (<system error>).
 * ac_warn_perr(msg):  Warning: <msg> (<system error>).
 * ac_err_perr(""):      Error (<system error>).
 * ac_err_perr(msg):   Error: <msg> (<system error>).
 *
 * ac_piep, ac_pieprf, etc. (pronounced 'peep'): Intended as
 * easy-to-type way to warn that something's wrong (and possibly return
 * false, return 0, etc.). Pipes through warn().
 *
 * ac_ipiep, ac_ipieprf, etc.: similar, except using iwarn().
 *
 * ac_idie(...): calls ac_ierr(...) and exits.
 * ac_die(...): calls ac_err(...) and exits.
 *
 * The fatal error functions will often hold a few open pointers upon
 * program exit. It's too difficult (and overkill) to try to free them
 * before exit (they could be static).
 *
 * It is safe to omit the braces in if/else statements:
 *  if (...) ac_warn (...);
 *  else ac_ierr_perr (...);
 *
 */

#define ac_iwarn(format...) do { \
  ac_complain(__FILE__, __LINE__, __func__, false, false, format); \
} while (0)

#define ac_ierr(format...) do { \
  ac_complain(__FILE__, __LINE__, __func__, true, false, format); \
} while (0)

#define ac_die(...) do { \
  ac_err(__VA_ARGS__); \
  exit(1); \
} while (0)

#define ac_idie(...) do { \
  ac_ierr(__VA_ARGS__); \
  exit(1); \
} while (0)

#define ac_iwarn_perr(format...) do { \
  ac_complain(__FILE__, __LINE__, __func__, false, true, format); \
} while (0)

#define ac_ierr_perr(format...) do { \
  ac_complain(__FILE__, __LINE__, __func__, true, true, format); \
} while (0)

#define ac_warn(format...) do { \
  ac_complain("", 0, __func__, false, false, format); \
} while (0)

#define ac_warn_rf(args...) do { \
  ac_warn(args); \
  return false; \
} while (0)

#define ac_warn_rn(args...) do { \
  ac_warn(args); \
  return NULL; \
} while (0)

#define ac_warn_perr_rf(args...) do { \
  ac_warn_perr(args); \
  return false; \
} while (0)

#define ac_warn_perr_rn(args...) do { \
  ac_warn_perr(args); \
  return NULL; \
} while (0)

#define ac_warn_perr(format...) do { \
  ac_complain("", 0, __func__, false, true, format); \
} while (0)

#define ac_err(format...) do { \
  ac_complain("", 0, __func__, true, false, format); \
} while (0)

#define ac_err_perr(format...) do { \
  ac_complain("", 0, __func__, true, true, format); \
} while (0)

#define ac_err_rf(args...) do { \
  ac_err(args); \
  return false; \
} while (0)

#define ac_err_rn(args...) do { \
  ac_err(args); \
  return NULL; \
} while (0)

#define ac_err_perr_rf(args...) do { \
  ac_err_perr(args); \
  return false; \
} while (0)

#define ac_err_perr_rn(args...) do { \
  ac_err_perr(args); \
  return NULL; \
} while (0)

#define ac_ipiep do { \
  ac_iwarn(""); \
} while (0)

#define ac_ipiep_rf do { \
  ac_ipiep; \
  return false; \
} while (0)

#define ac_ipiep_rt do { \
  ac_ipiep; \
  return true; \
} while (0)

#define ac_ipiep_r1 do { \
  ac_ipiep; \
  return 1; \
} while (0)

#define ac_ipiep_rv do { \
  ac_ipiep; \
  return; \
} while (0)

#define ac_ipiep_rn do { \
  ac_ipiep; \
  return NULL; \
} while (0)

#define ac_ipiep_rneg1 do { \
  ac_ipiep; \
  return -1; \
} while (0)

#define ac_ipiep_r0 do { \
  ac_ipiep; \
  return 0; \
} while (0)

#define ac_piep do { \
  ac_warn(""); \
} while (0)

#define ac_piep_rf do { \
  ac_piep; \
  return false; \
} while (0)

#define ac_piep_rt do { \
  ac_piep; \
  return true; \
} while (0)

#define ac_piep_r1 do { \
  ac_piep; \
  return 1; \
} while (0)

#define ac_piep_rv do { \
  ac_piep; \
  return; \
} while (0)

#define ac_piep_rn do { \
  ac_piep; \
  return NULL; \
} while (0)

#define ac_piep_rneg1 do { \
  ac_piep; \
  return -1; \
} while (0)

#define ac_piep_r0 do { \
  ac_piep; \
  return 0; \
} while (0)
#endif

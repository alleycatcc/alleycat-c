#ifdef _ALLEYCAT_H
#else
#define _ALLEYCAT_H

#include <stdbool.h>
#include <stdio.h>

#include "private/mem-macros.h"
#include "private/squeak-macros.h"

#define AC_DEBUG_LENGTH 200

/** colors.h
 */
char *ac_m_bright_blue (const char *s);
char *ac_m_red (const char *s);
char *ac_m_bright_red (const char *s);
char *ac_m_green (const char *s);
char *ac_m_bright_green (const char *s);
char *ac_m_yellow (const char *s);
char *ac_m_bright_yellow (const char *s);
char *ac_m_blue (const char *s);
char *ac_m_bright_blue (const char *s);
char *ac_m_cyan (const char *s);
char *ac_m_bright_cyan (const char *s);
char *ac_m_magenta (const char *s);
char *ac_m_bright_magenta (const char *s);

void ac_red (const char *s);
void ac_bright_red (const char *s);
void ac_green (const char *s);
void ac_bright_green (const char *s);
void ac_yellow (const char *s);
void ac_bright_yellow (const char *s);
void ac_blue (const char *s);
void ac_bright_blue (const char *s);
void ac_cyan (const char *s);
void ac_bright_cyan (const char *s);
void ac_magenta (const char *s);
void ac_bright_magenta (const char *s);

void ac_disable_colors ();
void ac_enable_colors ();
void ac_force_colors ();
void ac_unforce_colors ();

/** mem.h
 */
void *ac_malloc (size_t s);
void *ac_malloc_non_fatal (size_t s);
void *ac_calloc (size_t nmemb, size_t size);
void *ac_calloc_non_fatal (size_t nmemb, size_t size);

/** speak.h
 */
void ac_info (const char *format, ...);

/** squeak.h
 */
char *ac_perr ();
void ac_complain (const char *file, unsigned int line, const char *func, bool iserr, bool do_perr, const char *format, ...);

/** string.h
*/
extern char * const ac_s, * const ac_t, * const ac_u, * const ac_v,
            * const ac_w, * const ac_x, * const ac_y, * const ac_z;
void ac_ ();
void ac_spr (const char *format, ...);

void ac_static_strings_save ();
void ac_static_strings_restore ();

/** util.h
*/
void ac_autoflush (FILE *handle);

/* ##__VA_ARGS__ to swallow the preceding comma if omitted.
 */

#ifdef AC_DEBUG
# define ac_debug(x, ...) do { \
    char *dbg = ac_malloc (AC_DEBUG_LENGTH * sizeof (char)); \
    snprintf (dbg, AC_DEBUG_LENGTH, x, ##__VA_ARGS__); \
    ac_info("* [debug] %s", dbg); \
    free(dbg); \
} while (0)
#else
# define ac_debug(...) {}
#endif

// --- @todo only used in this file.
char *str (size_t length);

#endif

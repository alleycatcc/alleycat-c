## v0.3.1

*2023-08-20*

- all macros which use the do/while (0) convention now have no semicolon in
  the definition, meaning the caller must add one.

## v0.3.0

*2023-08-20*

- memory macros have been renamed from try_xxx to ac_try_xxx
- the calling style is now like this:

      ac_try_malloc (struct state *, the_state)

  where it used to be like this:
      
      ac_try_malloc (struct state, the_state)

  The new way should be easier to read, especially with for example calloc
  and several asterisks. Also, it makes it possible to use these macros with
  opaque types:

      ac_try_malloc (state_t, the_state)

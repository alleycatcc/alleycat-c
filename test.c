#include <stdlib.h>
#include <stdio.h>

#include "include/alleycat.h"

int main (int argc, char **argv) {
  ac_ ();
  ac_bright_red ("bad");
  ac_bright_green ("good");
  ac_spr ("forty %d", 2);
  printf("hello? %s %s %s\n", ac_s, ac_t, ac_u);

  ac_info ("1234567");
  ac_info ("123456");
  ac_info ("12345");
  ac_info ("1234");

  ac_warn ("warning");
  ac_ierr ("real bad");
  ac_iwarn ("iwarning");

  return 0;
}

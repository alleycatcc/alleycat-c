// --- char *strerror_r ()
#define _GNU_SOURCE

#include <errno.h>
#include <stdarg.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "include/private/colors.h"
#include "include/private/mem.h"
#include "include/private/squeak.h"
#include "include/private/squeak-macros.h"
#include "include/private/string.h"
#include "include/private/util.h"

#define PERR_STRING_SIZE 100
static const int COMPLAINT_LENGTH = 500;
static char perr_string[PERR_STRING_SIZE * sizeof (char)];

static void get_warn_prefix (const char *filename, int line, const char *func, size_t n, char *ret) {
  ac_static_strings_save ();
  ac_ ();
  ac_spr ("%d", line);
  ac_yellow (ac_s);
  snprintf (ret, n, "%s:%s:%s()", filename, ac_t, func);
  ac_static_strings_restore ();
}

/* Needs to be exported so that macros in squeak-macros.h work.
 */
void ac_complain (const char *file, unsigned int line, const char *func, bool iserr, bool do_perr, const char *format, ...) {
  int const en = errno;
  char * const the_str = ac_malloc (COMPLAINT_LENGTH * sizeof (char));

  va_list arglist;
  va_start (arglist, format);
  int rc = vsnprintf (the_str, COMPLAINT_LENGTH, format, arglist);
  if (rc >= COMPLAINT_LENGTH) fprintf (stderr,
    "ac_complain (): message string truncated"
  );
  va_end (arglist);

  char fl[100];
  char *p = NULL; // perr
  const char * const bull = get_bullet ();
  char * const bullet = iserr ? ac_m_red (bull) : ac_m_bright_red (bull);

  char *t1 = "";
  char *t2 = "";
  char *t3 = "";
  char *t4 = "";
  char *t5 = "";
  char *t6 = "";
  char *t7 = "";

  /* see comments in squeak-macros.h for all the cases.
   */
  if (file && line) {
    get_warn_prefix (file, line, func, 100, fl);
    t1 = fl;
    t2 = " ";
    if (iserr) {
      if (do_perr) {
        errno = en;
        p = ac_m_bright_red ((char*) ac_perr ()); // ditch const
        // ac_ierr_perr (msg)
        if (*the_str) {
          t3 = "Internal error: ";
          t4 = the_str;
          t5 = " (";
          t6 = p;
          t7 = ").";
        }
        // ac_ierr_perr ()
        else {
          t3 = "Internal error (";
          t4 = p;
          t5 = ").";
        }
      }
      else {
        // ierr (msg)
        if (*the_str) {
          t3 = "Internal error: ";
          t4 = the_str;
        }
        // ierr ()
        else {
          t3 = "Internal error.";
        }
      }
    }
    else {
      if (do_perr) {
        errno = en;
        p = ac_m_bright_red ((char*) ac_perr ()); // ditch const
        // ac_iwarn_perr (msg)
        if (*the_str) {
          t3 = "Internal warning: ";
          t4 = the_str;
          t5 = " (";
          t6 = p;
          t7 = ").";
        }
        // ac_iwarn_perr ()
        else {
          t3 = "Something's wrong (internally) (";
          t2 = p;
          t3 = ").";
        }
      }
      else {
        // ac_iwarn (msg)
        if (*the_str) {
          t3 = "Internal warning: ";
          t4 = the_str;
        }
        // ac_iwarn ()
        else {
          t3 = "Something's wrong (internally).";
        }
      }
    }
  }
  /* user/system warnings and errors
   */
  else {
    if (iserr) {
      if (do_perr) {
        errno = en;
        p = ac_m_bright_red ((char*) ac_perr ()); // ditch const
        // ac_err_perr (msg)
        if (*the_str) {
          t1 = "Error: ";
          t2 = the_str;
          t3 = " (";
          t4 = p;
          t5 = ").";
        }
        // ac_err_perr ()
        else {
          t1 = "Error (";
          t2 = p;
          t3 = ").";
        }
      }
      else {
        // ac_err (msg)
        if (*the_str) {
          t1 = "Error: ";
          t2 = the_str;
        }
        // ac_err ()
        else {
          t1 = "Error.";
        }
      }
    }
    else {
      if (do_perr) {
        errno = en;
        p = ac_m_bright_red ((char*) ac_perr ()); // ditch const
        // ac_warn_perr (msg)
        if (*the_str) {
          t1 = "Warning: ";
          t2 = the_str;
          t3 = " (";
          t4 = p;
          t5 = ").";
        }
        // ac_warn_perr ()
        else {
          t1 = "Something's wrong (";
          t2 = p;
          t3 = ").";
        }
      }
      else {
        // ac_warn (msg)
        if (*the_str) {
          t1 = "Warning: ";
          t2 = the_str;
        }
        // ac_warn ()
        else {
          t1 = "Something's wrong.";
        }
      }
    }
  }

  fprintf (stderr, "%s %s%s%s%s%s%s%s\n", bullet, t1, t2, t3, t4, t5, t6, t7);

  if (p) free (p);
  free (the_str);
  free (bullet);
}

/* Returns either a static pointer to a string (don't free it), in which
 * case buf is unused, or a pointer to the string stored in buf. In the
 * second case, does it need to be freed? XX
 */

char *ac_perr () {
  // --- GNU: return char* instead of int. `ret` may point to a static string, so return it instead
  // of returning `perr_string`.
  return strerror_r (errno, perr_string, PERR_STRING_SIZE);
}

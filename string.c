#include <assert.h>
#include <stdarg.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "include/private/string.h"
#include "include/private/mem.h"

#define NUM_STATIC_STRINGS 8
// --- including \0.
#define STATIC_STR_LENGTH 200

char _ac_s[STATIC_STR_LENGTH];
char _ac_t[STATIC_STR_LENGTH];
char _ac_u[STATIC_STR_LENGTH];
char _ac_v[STATIC_STR_LENGTH];
char _ac_w[STATIC_STR_LENGTH];
char _ac_x[STATIC_STR_LENGTH];
char _ac_y[STATIC_STR_LENGTH];
char _ac_z[STATIC_STR_LENGTH];

char * const ac_s = _ac_s;
char * const ac_t = _ac_t;
char * const ac_u = _ac_u;
char * const ac_v = _ac_v;
char * const ac_w = _ac_w;
char * const ac_x = _ac_x;
char * const ac_y = _ac_y;
char * const ac_z = _ac_z;

static char *static_strs[NUM_STATIC_STRINGS] = {
  ac_s, ac_t, ac_u, ac_v,
  ac_w, ac_x, ac_y, ac_z,
};

static int static_str_idx = -1;

static char *get_static_str_ptr () {
  static_str_idx = (static_str_idx+1) % NUM_STATIC_STRINGS;
  return static_strs[static_str_idx];
}

static void static_strings_save_restore (bool save) {
  static char *saves, *savet, *saveu, *savev, *savew;
  static bool saved = false;

  if (save) {
    saves = ac_malloc (sizeof (char) * STATIC_STR_LENGTH);
    savet = ac_malloc (sizeof (char) * STATIC_STR_LENGTH);
    saveu = ac_malloc (sizeof (char) * STATIC_STR_LENGTH);
    savev = ac_malloc (sizeof (char) * STATIC_STR_LENGTH);
    savew = ac_malloc (sizeof (char) * STATIC_STR_LENGTH);
    memcpy (saves, ac_s, STATIC_STR_LENGTH);
    memcpy (savet, ac_t, STATIC_STR_LENGTH);
    memcpy (saveu, ac_u, STATIC_STR_LENGTH);
    memcpy (savev, ac_v, STATIC_STR_LENGTH);
    memcpy (savew, ac_w, STATIC_STR_LENGTH);

    saved = true;
    return;
  }

  if (!saved) return;

  memcpy (ac_s, saves, STATIC_STR_LENGTH);
  memcpy (ac_t, savet, STATIC_STR_LENGTH);
  memcpy (ac_u, saveu, STATIC_STR_LENGTH);
  memcpy (ac_v, savev, STATIC_STR_LENGTH);
  memcpy (ac_w, savew, STATIC_STR_LENGTH);

  saved = false;
}

void ac_ () {
  static_str_idx = -1;
  for (int i = 0; i < NUM_STATIC_STRINGS; i++)
    memset (static_strs[i], '\0', STATIC_STR_LENGTH);
}

/* Store the string with printf-style format in the next static string, i.e.
 * `ac_s`, `ac_t` etc.
 * Result is null-terminated, even if size too small.
 */
void ac_spr (const char *format, ...) {
  int const size = STATIC_STR_LENGTH;
  char * const s = ac_malloc (size * sizeof (char));
  va_list arglist;
  va_start (arglist, format);
  int rc = vsnprintf (s, size, format, arglist);
  va_end (arglist);

  if (rc >= size) fprintf (stderr,
    "Warning: ac_spr (): static string truncated (%s)\n", s
  );
  memcpy (get_static_str_ptr (), s, size);
  free (s);
}

void ac_static_strings_restore () {
  static_strings_save_restore (false);
}

void ac_static_strings_save () {
  static_strings_save_restore (true);
}

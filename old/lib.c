/*
 * Author: Allen Haim <allen@netherrealm.net>, © 2015.
 * Source: github.com/misterfish/fish-lib-util
 * Licence: GPL 2.0
 */

#define _GNU_SOURCE // here, not header, not exported

/* For errors and warnings.
 */
#define COMPLAINT_LENGTH F_COMPLAINT_LENGTH
#define INFO_LENGTH 300

#define SOCKET_LENGTH_DEFAULT 100

// length of (longest) color escape
#define COLOR_LENGTH 5
#define COLOR_LENGTH_RESET 4

#define unknown_sig(signal) "Signal number " #signal // stringify
#define signame_(n, d) do { \
  if (name) *name = n; \
  if (desc) *desc = d; \
} while (0)

#include <locale.h>

#include <ctype.h>  // isdigit
#include <signal.h>
// errno
#include <errno.h>

#include <sys/timeb.h>

// LONG_MIN
#include <limits.h>

#include <string.h>

#include <stdlib.h>
// varargs
#include <stdarg.h>

#include <stdio.h>
#include <sys/time.h>
#include <math.h>

#include <assert.h>

// dirname.
#include <libgen.h>

// offsetof
#include <stddef.h>

#include <sys/socket.h>
#include <sys/un.h>

#include <wchar.h>

/* stat */
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h> // isatty
/* */

#include "fish-util.h"

/* Private headers.
 */

static char **_get_static_str_ptr ();
static char *_color (const char *s, int idx);
static void _color_static (const char *c);
static void _sys_say (const char *cmd);
static void _static_strings_free ();

/* Public.
 */

/* Private.
 */

static struct stat *mystat;
static bool mystat_initted = false;

static bool _die = false;
static bool _verbose = true;

/* Returns random double from [0, r).
 * Caller should check for overflows etc.
 * Don't use this function, ever, at all. Only sometimes.
 */
double get_random (int r) {
  struct timeval tv = {0};
  if (gettimeofday (&tv, NULL)) {
    ac_warn_perr ("Can't call gettimeofday");
    return -1;
  }
  srandom (tv.tv_sec * tv.tv_usec);
  // RAND_MAX ~ 31 bits
  long int rand = random ();
  if (rand == RAND_MAX)
    rand--;
  double rnd = rand * 1.0 / RAND_MAX * r;
  if (rnd == r) {
    ac_iwarn ("Error making random number.");
    return -1;
  }
  return rnd;
}

/* Public functions.
 */

/* glibc version of dirname segfaults with static strings.
 * This version does a strdup first.
 * Caller should not free.
 */
char *f_dirname (const char *s) {
  if (!s) {
    ac_iwarn ("f_dirname (): null arg");
    return NULL;
  }
  char *t = f_strdup (s);
  char *ret = dirname (t);
  free (t);
  return ret;
}

void *f_calloc (size_t nmemb, size_t size) {
  void *ptr = calloc (nmemb, size);
  if (!ptr && nmemb && size) // NULL ok if size or nmemb is 0
    oom_fatal ();
  return ptr;
}

void *f_realloc (void *ptr, size_t size) {
  void *new = realloc (ptr, size);
  if (!new && size) // NULL can mean size is 0
    oom_fatal ();
  return new;
}

/* Caller shouldn't free.
 */
FILE *safeopen_f (const char *filespec, int flags) {
  FILE *f = NULL;

  const char *filename = NULL;
  bool is_err = false;
  bool i_die = ! (flags & F_NODIE);
  bool quiet = flags & F_QUIET;
  bool utf8 = flags & F_UTF8;
  char *mode;
  char *mode_f = str (3 + 10 + 1); // max mode 3 long, including b, 10 for utf-8

  if (flags & F_WRITE) {
    mode = "writing";
    strcpy (mode_f, "w");
    filename = filespec;
  }
  else if (flags & F_READ_WRITE_NO_TRUNC) {
    mode = "read-write, no create or truncate";
    strcpy (mode_f, "r+");
    filename = filespec;
  }
  else if (*filespec == '+') {
    mode = "read-write, no create or truncate";
    strcpy (mode_f, "r+");
    filename = filespec + 1;
  }
  else if (
      (*filespec == '<' && *(filespec+1) == '+') ||
      (*filespec == '+' && *(filespec+1) == '<')
    ) {
    mode = "read-write, no create or truncate";
    strcpy (mode_f, "r+");
    filename = filespec + 2;
  }
  else if (flags & F_READ_WRITE_TRUNC) {
    mode = "read-write, create or truncate";
    strcpy (mode_f, "w+");
    filename = filespec;
  }
  else if (
      (*filespec == '>' && *(filespec+1) == '+') ||
      (*filespec == '+' && *(filespec+1) == '>')
    ) {
    mode = "read-write, create or truncate";
    strcpy (mode_f, "w+");
    filename = filespec + 2;
  }
  else if (*filespec == '>' && *(filespec+1) == '>') {
    mode = "appending";
    strcpy (mode_f, "a");
    filename = filespec + 2;
  }
  else if (*filespec == '>') {
    mode = "writing";
    strcpy (mode_f, "w");
    filename = filespec + 1;
  }
  else if (flags & F_APPEND) {
    mode = "appending";
    strcpy (mode_f, "a");
    filename = filespec;
  }
  else if (flags & F_READ) {
    mode = "reading";
    strcpy (mode_f, "r");
    filename = filespec;
  }
  else if (*filespec == '<') {
    mode = "reading";
    strcpy (mode_f, "r");
    filename = filespec + 1;
  }
  else {
    mode = "reading";
    strcpy (mode_f, "r");
    filename = filespec;
  }

  if (utf8) {
    sprintf (mode_f, "%s,ccs=UTF-8", mode_f); // NO space after comma, before is ok
  }

  if (f_test_d (filename)) {
    _ ();
    Y (filename);
    ac_iwarn ("File %s is a directory (can't open)", _s);
    is_err = true;
  }
  else {
    f = fopen (filename, mode_f);
    if (!f) {
      int en = errno;
      is_err = true;
      if (!quiet) {
        _ ();
        Y (filename);
        CY (mode);
        errno = en;
        ac_iwarn ("Couldn't open %s for %s: %s", _s, _t, perr ());
      }
    }
  }

  free (mode_f);

  if (is_err) {
    if (i_die) {
      exit (1);
    }
    else {
      return NULL;
    }
  }
  return f;
}

FILE *safeopen (const char *filespec) {
  return safeopen_f (filespec, 0);
}

bool f_test_f (const char *file) {
  struct stat *s = f_stat_f (file, F_QUIET);
  if (!s)
    return false;
  int mode = s->st_mode;
  if (!mode)
    return false;

  return S_ISREG (mode);
}

bool f_test_d (const char *file) {
  struct stat *s = f_stat_f (file, F_QUIET);
  if (!s)
    return false;
  int mode = s->st_mode;
  if (!mode)
    return false;

  return S_ISDIR (mode);
}

/* Take from the heap, probably slower than normal stat.
 */
struct stat *f_stat_f (const char *file, int flags) {
  if (!mystat_initted) {
    mystat = f_mallocv (*mystat);
    mystat_initted = true;
  }
  memset (mystat, 0, sizeof (*mystat));

  bool quiet = flags & F_QUIET;

  if (-1 == stat (file, mystat)) {
    int en = errno;
    if (!quiet) {
      _ ();
      Y (file);
      errno = en;
      ac_warn_perr ("Couldn't stat %s: %s", _s);
    }
    return NULL;
  }
  return mystat;
}

struct stat *f_stat (const char *file) {
  return f_stat_f (file, 0);
}

void ask (const char *format, ...) {
  char *new = str (INFO_LENGTH);
  char *new2 = str (INFO_LENGTH + 1);
  va_list arglist, arglist_copy;
  va_start ( arglist, format );
  va_copy (arglist_copy, arglist);
  // get rid of new2, just check rc XX
  vsnprintf ( new, INFO_LENGTH, format, arglist );
  vsnprintf ( new2, INFO_LENGTH + 1, format, arglist_copy );
  if (strncmp (new, new2, INFO_LENGTH)) { // no + 1 necessary
    ac_warn ("Ask string truncated.");
  }
  va_end ( arglist );

  char *bullet = get_bullet ();
  char *question = str (strlen (new) + COLOR_LENGTH + COLOR_LENGTH_RESET + strlen (bullet) + 1 + 1 + 1 + 1);
  char *c = M_ (bullet);
  sprintf (question, "%s %s? ", c, new);
  printf (question);
  free (c);
  free (question);

  free (new);
  free (new2);
}

void f_sys_die (bool b) {
  _die = b;
}

void f_verbose_cmds (bool b) {
  _verbose = b;
}

/* Non-null FILE means fork or pipe succeeded, but command could still fail
 * (e.g. command not found).
 * Caller should check for NULL, and if non-null, call sysclose to finish
 * (not free ()).
 */
FILE *sysr (const char *cmd) {
  if (_verbose) _sys_say (cmd);
  FILE *f = popen (cmd, "r");
  errno = -1;
  if (!f) {
    /* memory allocation failed (errno doesn't get set).
     */
    if (errno == -1)
      oom_fatal ();
    int e = errno;
    _ ();
    BR (cmd);
    spr ("Can't launch cmd (%s) for reading, fork or pipe failed.", _s);
    errno = e;
    if (_die)
      err_perr (_t);
    else
      warn_perr (_t);
  }
  return f;
}

/* Non-null FILE means fork or pipe succeeded, but command could still fail
 * (e.g. command not found).
 * Caller should check for NULL, and if non-null, call sysclose to finish
 * (not free ()).
 */
FILE *sysw (const char *cmd) {
  if (_verbose) _sys_say (cmd);
  errno = -1;
  FILE *f = popen (cmd, "w");
  if (f == NULL) {
    /* memory allocation failed (errno doesn't get set).
     */
    if (errno == -1)
      oom_fatal ();
    int e = errno;
    _ ();
    BR (cmd);
    spr ("Can't launch cmd (%s) for reading, fork or pipe failed.", _s);
    errno = e;
    if (_die)
      err_perr (_t);
    else
      warn_perr (_t);
  }
  return f;
}

/* 0 means good, -1 means wait or another sys call failed, > 0 is a non-zero
 * exit from the command.
 * The cmd arg is optional -- with it we can make a nicer error message.
 *
 * This will print an error message if a popen'ed command is closed while
 * there's still data in the buffer. Use flag F_QUIET to silence this.
 */
int sysclose_f (FILE *f, const char *cmd, int flags) {
  bool quiet = flags && F_QUIET;
  int status = pclose (f);
  if (status) {
    int en = errno;
    _ ();
    if (cmd && *cmd != '\0') {
      BR (cmd);
      spr (" «%s»", _s);
    }
    else {
      spr ("");
      spr ("");
    }
    /* On linux and most unices, the 7 least significant bits contain
     * the signal, and the 8th tells us if we core dumped, if it was
     * killed by a signal.
     * We will assume we are on one of those systems.
     */
    int signal = status & 0x7F;
    int core_dumped = status & 0x80;
    if (signal) {
      char *name;
      f_signame (signal, &name, NULL);
      CY (name);
      spr (" «got signal %s»", _u);
    }
    else {
      spr ("");
      spr ("");
    }
    if (core_dumped) {
      M ("core dumped");
      spr (" «%s»", _w);
    }
    else {
      spr ("");
      spr ("");
    }

    status >>= 8;
    spr ("%d", status);
    Y (_y);
    // XX
    char *msg = spr_ ("Error closing cmd%s «exit status %s»%s%s.", 300, _t, _z, _v, _x);
    if (status == -1) {
      errno = en;
      if (_die)
        err_perr (msg);
      else if (!quiet)
        warn_perr (msg);
    }
    else {
      if (_die)
        err (msg);
      else if (!quiet)
        ac_warn (msg);
    }
    free (msg);
  }
  return status;
}

int sysclose (FILE *f) {
  return sysclose_f (f, NULL, 0);
}

/* Run command and read input until EOF.
 * Returns the cmd status, or -1 on failure.
 * To not read the input, use sysr.
 */
int sys (const char *cmd) {
  FILE *f = sysr (cmd);
  /* Leave complaining to sysr.
   */
  if (!f)
    return -1;

  int rc;
  char buf[100];
  errno = -1;
  while (fgets (buf, 100, f)) {
  }
  if (ferror (f)) {
    /* Not clear whether fgets is required to set errno.
     */
    if (errno != -1)
      warn_perr ("Interrupted read from command");
    else
      ac_warn ("Interrupted read from command");
    return -1;
  }

  return sysclose_f (f, cmd, 0);
}

bool f_sig (int signum, void *func) {
  /* Ok that it's thrown away.
   */
  struct sigaction action = {0};
  action.sa_handler = func;
  int rc = sigaction (signum, &action, NULL);
  if (rc) {
    int en = errno;
    _ ();
    errno = en;
    spr ("%s", perr ());
    spr ("%d", signum);
    R (_t);
    ac_warn ("Couldn't attach signal %s: %s", _u, _s);
    return false;
  }
  return true;
}

bool f_socket_make_named (const char *filename, int *socknum) {
  struct sockaddr_un name;
  size_t size;

  // 0 for protocol
  int sock = socket (AF_UNIX, SOCK_STREAM | SOCK_NONBLOCK, 0);
  if (sock < 0) {
    int en = errno;
    _ ();
    Y (filename);
    errno = en;
    ac_warn ("Can't make socket %s: %s", _s, perr ());
    return false;
  }

  name.sun_family = AF_UNIX;
  strncpy (name.sun_path, filename, sizeof (name.sun_path));
  name.sun_path[sizeof (name.sun_path) - 1] = '\0';

  /* The size of the address is
     the offset of the start of the filename,
     plus its length,
     plus one for the terminating null byte.
     Alternatively you can just do:
     size = SUN_LEN (&name);
   */
  size = (offsetof (struct sockaddr_un, sun_path) + strlen (name.sun_path) + 1);

  if (bind (sock, (struct sockaddr *) &name, size) < 0) {
    int en = errno;
    _ ();
    Y (filename);
    errno = en;
    ac_warn ("Can't bind socket %s (%s)", _s, perr ());
    return false;
  }

  *socknum = sock;
  return true;
}

bool f_socket_make_client (int socket, int *client_socket) {
  /* Apparently ok to throw away.
   */
  struct sockaddr a;
  socklen_t t = sizeof (a);
  int cs = accept (socket, &a, &t);
  if (cs == -1) {
    int en = errno;
    _ ();
    spr ("%d", socket);
    Y (_s);
    errno = en;
    ac_warn ("Couldn't make client socket for socknum %s (%s)", _t, perr ());
    return false;
  }
  if (client_socket != NULL)
    *client_socket = cs;
  return true;
}

bool f_socket_read (int client_socket, ssize_t *num_read, char *buf, size_t max_length) {
  ssize_t rc = recv (client_socket, buf, max_length, 0); // blocking
  if (rc == -1) {
    int en = errno;
    _ ();
    spr ("%d", client_socket);
    Y (_s);
    errno = en;
    ac_warn ("Couldn't read from socket %s (%s)", _t, perr ());
    return false;
  }
  if (num_read != NULL)
    *num_read = rc;
  return true;
}

bool f_socket_write (int client_socket, ssize_t *num_written, const char *buf, size_t len) {
  ssize_t rc = write (client_socket, buf, len);
  if (rc == -1) {
    int en = errno;
    _ ();
    spr ("%d", client_socket);
    Y (_s);
    errno = en;
    ac_warn ("Couldn't write to socket %s (%s)", _t, perr ());
    return false;
  }

  if (num_written != NULL)
    *num_written = rc;

  return true;
}

bool f_socket_close (int client_socket) {
  if (close (client_socket)) {
    int en = errno;
    _ ();
    spr ("%d", client_socket);
    Y (_s);
    errno = en;
    ac_warn ("Couldn't close client socket %s (%s)", _t, perr ());
    return false;
  }
  return true;
}

/* strlen filename, strlen msg.
 * buf_length includes \0.
 * msg checked against buf_length.
 * response not checked for size -- caller should ssure it is buf_length
 * bytes big.
 */
bool f_socket_unix_message_f (const char *filename, const char *msg, char *response, int buf_length) {
  struct sockaddr_un serv_addr;
  memset (&serv_addr, 0, sizeof (serv_addr));

  bool trash_response = response == NULL;

  int msg_len = strlen (msg); // -O- trust caller
  int filename_len = strlen (filename); // -O- trust caller

  // both leak on early return XX
  char *filename_color = CY_ (filename);
  char *msg_s = str (msg_len + 1 + 1);

  sprintf (msg_s, "%s\n", msg);

  if (strlen (msg) >= buf_length) {
    _ ();
    spr ("%d", buf_length - 1);
    CY (_s);

    ac_iwarn ("msg too long (max is %s)", _t);
    return false;
  }

  // 0 means choose protocol automatically
  int sockfd = socket (AF_UNIX, SOCK_STREAM, 0);

  if (sockfd < 0) {
    ac_iwarn ("Error opening socket %s: %s", filename_color, perr ());
    return false;
  }

  serv_addr.sun_family = AF_UNIX;
  strncpy (serv_addr.sun_path, filename, filename_len);

  if (connect (sockfd, (struct sockaddr *) &serv_addr, sizeof (serv_addr) ) < 0) {
    ac_iwarn_perr ("Error connecting to socket %s", filename_color);
    return false;
  }

  int rc = write (sockfd, msg_s, msg_len + 1);

  if (rc < 0) {
    ac_iwarn_perr ("Error writing to socket %s", filename_color);
    return false;
  }

  /* Read 1 char at a time, and stop on newline or EOF.
   * Not efficient, but easy to program.
   * Note that a short read is not an error.
   */
  char c[1];
  int i;
  for (i = 0; i < buf_length; i++) {
    ssize_t numread = read (sockfd, c, 1);
    if (numread < 0) {
      ac_iwarn_perr ("Error reading from socket %s", filename_color);
      return false;
    }
    if (*c == '\n')
      break;
    if (!trash_response)
      *response++ = *c;
  }
  *response = '\0';

  if (close (sockfd)) {
    ac_iwarn_perr ("Error closing socket fd %d", sockfd);
    return false;
  }

  free (filename_color);
  free (msg_s);

  return true;
}

bool f_socket_unix_message (const char *filename, const char *msg) {
  //return f_socket_unix_message_f (filename, msg, NULL, SOCKET_LENGTH_DEFAULT + 1);
  return f_socket_unix_message_f (filename, msg, NULL, SOCKET_LENGTH_DEFAULT);
}

double f_time_hires () {
  struct timeb t;
  memset (&t, 0, sizeof (t));
  ftime (&t);

  time_t time = t.time;
  unsigned short millitm = t.millitm;

  double c = time + millitm / 1000.0;
  return c;
}

bool f_yes_no_flags (int deflt, int flags) {
  bool infinite = ! (flags & F_NOINFINITE); // def true

  bool ret = false;
  bool loop = true;
  while (loop) {
    char *yn =
      deflt == F_DEFAULT_YES ?  "(Y/n)" :
      deflt == F_DEFAULT_NO ? "(y/N)" :
      "(y/n)";
    printf ("%s ", yn);

    char *l = NULL;
    size_t n = 0;

    int rc = getline (&l, &n, stdin);
    f_chop (l);

    if (rc == -1 || !strcmp (l, "\0")) { // no input
      if (deflt == F_DEFAULT_YES) {
        ret = true;
        loop = false;
      }
      else if (deflt == F_DEFAULT_NO) {
        ret = false;
        loop = false;
      }
    }
    else {
      if (
        !strcmp (l, "no\0") ||
        !strcmp (l, "n\0")
      ) {
        ret = false;
        loop = false;
      }
      else if (
        !strcmp (l, "yes\0") ||
        !strcmp (l, "y\0")
      ) {
        ret = true;
        loop = false;
      }
    }

    free (l);
    if (!infinite) loop = false;
  }
  return ret;
}

bool f_yes_no () {
  return f_yes_no_flags (0, 0);
}

/* Max string size helps catch non-null terminated strings.
 * But if it's too big, it could cause a segfault, I believe; if they're
 * lucky, the function will stay alive but return null.
 */

char *f_field (int width, const char *string, int max_len) {
  // Max len gives some protection, not total.
  int len = strnlen (string, max_len);  // without \0

  if (len == max_len) {
    ac_warn ("field_with_len: max_len reached -- string not null terminated.");
    return NULL;
  }
  int num_spaces = width - len;
  if (num_spaces < 0) {
    ac_warn ("Field length (%d) bigger than desired width (%d)", len, width);
    num_spaces = 0;
  }
  char *new = str (len+num_spaces + 1); // comes with \0
  memset (new, ' ', len+num_spaces);
  memcpy (new, string, len);
  return new;
}

/* Not space-tolerant.
 * maxlen doesn't include \0, like strlen.
 */
bool f_is_int_strn (const char *s, int maxlen) {
  const char *t = s;
  int len = 1;
  if (*t != '+' && *t != '-' && *t != '0' && !isdigit (*t))
    return false;

  if (maxlen <= 0) {
    ac_warn ("f_is_int_strn: maxlen must be > 0 (got %d)", maxlen);
    return false;
  }

  while (++t, ++len, *t) {
    if (len > maxlen) break;
    if (!isdigit (*t)) return false;
  }
  return true;
}

/* Not space-tolerant.
 * f_is_int_strn is safer.
 */
bool f_is_int_str (const char *s) {
  const char *t = s;
  if (*t != '+' && *t != '-' && *t != '0' && !isdigit (*t))
    return false;
  while (++t, *t) {
    if (!isdigit (*t)) return false;
    //t++;
  }
  return true;
}

/* Caller has to assure \0-ok.
 */
void f_chop (char *s) {
  int i = strlen (s);
  if (i) s[i-1] = '\0';
  else ac_piep;
}

/* Caller has to assure \0-ok.
 */
void f_chop_w (wchar_t *s) {
  int i = wcslen (s);
  if (i) s[i-1] = L'\0';
  else ac_piep;
}

/* Caller should free.
 * len is the length without the \0 (in other words, the return value of
 * str (n)len).
 */
char *f_reverse_str (const char *orig, size_t len) {
  if (len <= 0) {
    ac_warn ("f_reverse_str: len should be positive");
    return NULL;
  }
  char *ret = ac_malloc ((len + 1) * sizeof (char));
  ret[len] = '\0';
  char *p = (char *)orig + len;
  while (--p >= orig) {
    *ret++ = *p;

  }
  return ret - len;
}

/* Caller should free.
 */
char *f_comma (long n) {
  int n_sz = f_int_length (n);
  char *n_as_str = str (n_sz + 1);
  /* *2 is more than enough for commas and \0.
   */
  size_t ret_r_sz = n_sz * 2 * sizeof (char);
  char *ret_r = str (ret_r_sz);
  if (snprintf (n_as_str, n_sz + 1, "%d", n) >= n_sz + 1)
    ac_pieprnull;
  int i;
  int j = 0;
  int k = -1;
  char *str_r = f_reverse_str (n_as_str, n_sz);
  for (i = 0; i < n_sz; i++) {
    char c = str_r[i];
    ret_r[++k] = c;

    if (++j == 3 && i != n_sz - 1) {
      j = 0;
      k++;
      ret_r[k] = ',';
    }
  }
  int bytes_written = k + 1; // not counting \0
  free (n_as_str);
  char *ret = f_reverse_str (ret_r, bytes_written);
  free (ret_r);
  free (str_r);
  return ret;
}

bool f_set_utf8_f (int flags) {
  int no_warn = flags & F_NOWARN;
  int verbose = flags & F_VERBOSE;
  char *l = setlocale (LC_ALL, ""); // get from env
  if (! l) {
    if (!no_warn)
      ac_warn ("Couldn't get lang or charset from env variables.");
    return false;
  }
  else {
    char *lang, *charset;
    if (strlen (l) < 5) {
      if (!no_warn)
        ac_warn ("Couldn't get lang or charset from env variables.");
      return false;
    }

    lang = str (6);
    strncpy (lang, l, 5);

    if (strlen (l) < 11) {
      if (!no_warn)
        ac_warn ("No charset in locale (%s), trying to set", l);
    }
    else if (strncmp (l + 6, "UTF-8", 5)) {
      if (!no_warn)
        ac_warn ("Charset in locale (%s) is not UTF-8, trying to set", l + 6);
    }
    else {
      // ok
      if (verbose)
        ac_info ("Set locale to %s", l);
      return true;
    }

    char *locale = str (12);

    sprintf (locale, "%s.UTF-8", lang);
    free (lang);

    char *m = setlocale (LC_ALL, locale);
    bool ok;
    if (! m) {
      if (!no_warn)
        ac_warn ("Couldn't set locale to %s", locale);
      ok = false;
    }
    else {
      ok = true;
      if (verbose)
        ac_info ("Set locale to %s", locale);
    }
    free (locale);
    return ok;

  }
  return true;
}

bool f_set_utf8 () {
  return f_set_utf8_f (0);
}

// caller should free
wchar_t *d8 (char *s) {
  int len = strlen (s);
  char *_line = str (len + 1);
  strcpy (_line, s);
  const char *line = _line; // doesn't need free
  wchar_t *line8 = ac_malloc (sizeof (wchar_t) * (len+1)); // overshoot
  memset (line8, L'\0', sizeof (wchar_t) * (len+1)); // valgrind complains without this, maybe ok though without it
  mbstate_t ps = {0};
  size_t num = mbsrtowcs (line8, &line, len /* at most this many wides are written, excl L\0 */, &ps); // consumes line but s untouched
  if (num == 0) {
    ac_warn ("d8: 0 wide chars written to dest string");
  }
  else if (num == -1) {
    ac_warn ("d8: unable to decode string: %s", perr ());
  }
  else {
    debug ("d8: converted %d chars", num);
  }
  free (_line);
  return line8;
}

int f_get_max_color_length () {
  return COLOR_LENGTH;
}

int f_get_color_reset_length () {
  return COLOR_LENGTH_RESET;
}

/* Don't free.
 */
void f_signame (int signal, char **name, char **desc) {
  switch (signal) {
    case SIGHUP:
      signame_ ("HUP", "Hangup detected on controlling terminal or death of controlling process");
      break;
    case SIGINT:
      signame_ ("INT", "Interrupt from keyboard");
      break;
    case SIGQUIT:
      signame_ ("QUIT", "Quit from keyboard");
      break;
    case SIGILL:
      signame_ ("ILL", "Illegal Instruction");
      break;
    case SIGABRT:
      signame_ ("ABRT", "Abort signal from abort (3)");
      break;
    case SIGFPE:
      signame_ ("FPE", "Floating point exception");
      break;
    case SIGKILL:
      signame_ ("KILL", "Kill signal");
      break;
    case SIGSEGV:
      /* signame_ ("SEGV", "Invalid memory reference");
       * better known as:
       */
      signame_ ("SEGV", "Segmentation fault");
      break;
    case SIGPIPE:
      signame_ ("PIPE", "Broken pipe: write to pipe with no readers");
      break;
    case SIGALRM:
      signame_ ("ALRM", "Timer signal from alarm (2)");
      break;
    case SIGTERM:
      signame_ ("TERM", "Termination signal");
      break;
    case SIGUSR1:
      signame_ ("USR1", "User-defined signal 1");
      break;
    case SIGUSR2:
      signame_ ("USR2", "User-defined signal 2");
      break;
    case SIGCHLD:
      signame_ ("CHLD", "Child stopped or terminated");
      break;
    case SIGCONT:
      signame_ ("CONT", "Continue if stopped");
      break;
    case SIGSTOP:
      signame_ ("STOP", "Stop process");
      break;
    case SIGTSTP:
      signame_ ("TSTP", "Stop typed at terminal");
      break;
    case SIGTTIN:
      signame_ ("TTIN", "Terminal input for background process");
      break;
    case SIGTTOU:
      signame_ ("TTOU", "Terminal output for background process");
      break;
    case SIGBUS:
      signame_ ("BUS", "Bus error (bad memory access)");
      break;
      /* synonym for SIGIO
    case SIGPOLL:
      signame_ ("POLL", "Pollable event (Sys V).");
      break;
      */
    case SIGPROF:
      signame_ ("PROF", "Profiling timer expired");
      break;
    case SIGSYS:
      signame_ ("SYS", "Bad argument to routine");
      break;
    case SIGTRAP:
      signame_ ("TRAP", "Trace/breakpoint trap");
      break;
    case SIGURG:
      signame_ ("URG", "Urgent condition on socket");
      break;
    case SIGVTALRM:
      signame_ ("VTALRM", "Virtual alarm clock");
      break;
    case SIGXCPU:
      signame_ ("XCPU", "CPU time limit exceeded");
      break;
    case SIGXFSZ:
      signame_ ("XFSZ", "File size limit exceeded");
      break;
      /* synonym for SIGABRT
    case SIGIOT:
      signame_ ("IOT", "IOT trap");
      break;
      */
/* these ifdefs won't work if they're not macros but normal ints.
 * works on linux in any case.
 */
#ifdef SIGEMT
    case SIGEMT:
      signame_ ("EMT", "");
      break;
#endif
#ifdef SIGSTKFLT
    case SIGSTKFLT:
      signame_ ("STKFLT", "Stack fault on coprocessor (unused)");
      break;
#endif
    case SIGIO:
      signame_ ("IO", "I/O now possible");
      break;
      /* synonym for SIGCHLD
    case SIGCLD:
      signame_ ("CLD", "Child stopped or terminated");
      break;
      */
    case SIGPWR:
      signame_ ("PWR", "Power failure");
      break;
      /* synonym for SIGPWR
    case SIGINFO:
      signame_ ("INFO", "Power failure");
      break;
      */
#ifdef SIGLOST
    case SIGLOST:
      signame_ ("LOST", "File lock lost");
      break;
#endif
    case SIGWINCH:
      signame_ ("WINCH", "Window resize signal");
      break;
      /* synonym for SIGSYS
    case SIGUNUSED:
      signame_ ("UNUSED", "Bad argument to routine");
      break;
      */
    default:
      if (signal >= SIGRTMIN && signal <= SIGRTMAX)
        signame_ ("RTMIN+n", "Real-time signal");
      else
        signame_ (unknown_sig (signal), "Unknown signal");
  }
}

static void _sys_say (const char *cmd) {
  char *bullet = get_bullet ();
  char *new = str (strlen (cmd) + strlen (bullet) + COLOR_LENGTH + COLOR_LENGTH_RESET + 2 + 1);
  char *c = G_ (bullet);
  sprintf (new, "%s %s", c, cmd);
  say (new);
  free (new);
  free (c);
}

// #define F_COMPLAINT_LENGTH 500

/* int (32 bits).
 * Not all combinations make sense and some are redundant.
 * Depends on function.

#define F_DIE              0x01
#define F_NODIE            0x02
#define F_QUIET            0x04
#define F_NOQUIET          0x08
#define F_KILLERR          0x10
#define F_NOKILLERR        0x20
#define F_UTF8             0x40
#define F_NOUTF8           0x80
#define F_WARN            0x100
#define F_NOWARN          0x200
#define F_VERBOSE         0x400
#define F_NOVERBOSE       0x800
#define F_DEFAULT_NONE   0x1000
#define F_DEFAULT_NO     0x2000
#define F_DEFAULT_YES    0x4000
#define F_INFINITE       0x8000
#define F_NOINFINITE    0x10000
#define F_WRITE         0x20000
#define F_READ          0x40000
#define F_APPEND        0x80000
#define F_READ_WRITE_NO_TRUNC   0x100000
#define F_READ_WRITE_TRUNC      0x200000
 */

/*
void *f_calloc (size_t nmemb, size_t size);
void *f_realloc (void *ptr, size_t size);

#define f_malloct(type) \
    ac_malloc(sizeof(type))

#define f_mallocv(var) \
    ac_malloc(sizeof var)

#define f_calloct(n, type) \
    f_calloc(n, sizeof(type))

#define f_callocv(n, var) \
    f_calloc(n, sizeof var)

#define f_realloct(ptr, type) \
    f_realloc(ptr, sizeof(type))

#define f_reallocv(ptr, var) \
    f_realloc(ptr, sizeof var)
    */

// void f_signame (int signal, char **name, char **desc);

/*
FILE *sysr (const char *cmd);
FILE *sysw (const char *cmd);

int sys (const char *cmd);
int sysclose (FILE *f);
int sysclose_f (FILE *f, const char *cmd, int flags);

FILE *safeopen (const char *filespec);
FILE *safeopen_f (const char *filespec, int flags);
*/

// wchar_t *d8 (char *s);

// void f_disable_colors ();
// void f_sys_die (bool b);
// void f_verbose_cmds (bool b);

// bool f_yes_no ();
// bool f_yes_no_flags (int, int);
// void f_autoflush ();
// bool f_sig (int signum, void *func);
// void f_benchmark ();

// bool f_atod (const char *s, double *ret);
// bool f_atoi (const char *s, int *ret);
// int f_int_length (long i);

// bool f_is_int_str (const char *s);
// bool f_is_int_strn (const char *s, int maxlen);
// void f_chop (char *s);
// void f_chop_w (wchar_t *s);
// char *f_comma (long n);

// bool f_set_utf8 ();
// bool f_set_utf8_f (int flags);

// int f_get_max_color_length ();
// int f_get_color_reset_length ();


# --- -rdynamic is necessary for backtraces to work.
# --- the application/library code which links to our library doesn't need
# the flag.

ar = $(AR) rs
shared = -shared
libs = -lm

cc = $(CC) -Wall -fPIC -rdynamic $(ccextra)

builddir-static = build/static
builddir-dynamic = build/dynamic
builddir-test = build/test

headers = include/alleycat.h \
          include/private/colors.h \
		  include/private/mem.h \
		  include/private/mem-macros.h \
		  include/private/speak.h \
		  include/private/squeak.h \
		  include/private/squeak-macros.h \
		  include/private/string.h \
		  include/private/util.h
src = colors.c mem.c speak.c squeak.c string.c util.c

all: $(builddir-static)/liballeycat.a \
	 $(builddir-dynamic)/liballeycat.so \
	 $(builddir-test)/test-static \
	 $(builddir-test)/test

prepare:
	mkdir -p $(builddir-static)
	mkdir -p $(builddir-dynamic)
	mkdir -p $(builddir-test)

# --- libs must come at end.
$(builddir-test)/test-static: $(builddir-static)/liballeycat.a test.c
	$(cc) test.c -o $(builddir-test)/test-static -static -L $(builddir-static) -lalleycat $(libs)

# --- use this for valgrind (static one shows spurious errors).
$(builddir-test)/test: $(builddir-dynamic)/liballeycat.so test.c
	$(cc) test.c -o $(builddir-test)/test -lalleycat -L $(builddir-dynamic) $(libs)

$(builddir-static)/liballeycat.a: $(src) $(headers)
	make prepare
	$(cc) -c $(src) $(libs)
	$(ar) $(builddir-static)/liballeycat.a *.o

$(builddir-dynamic)/liballeycat.so: $(src) $(headers)
	make prepare
	$(cc) $(src) -shared -o $(builddir-dynamic)/liballeycat.so

clean:
	rm -f *.o
	rm -rf $(builddir-static)
	rm -rf $(builddir-dynamic)
	rm -rf $(builddir-test)

mrproper: clean

.PHONY: all install clean mrproper prepare

#include <errno.h>
#include <math.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "include/private/util.h"

/*
 * Usage:
 *   #define AC_STRING_NAMESPACE some_ns
 *   // or for empty string:
 *   #define AC_STRING_NAMESPACE
 *
 *   void CONCAT (AC_STRING_NAMESPACE, foo) (arg1, ...) {
 *   // -> void some_ns_foo (arg1, ...) {;
 *
 * This double interpolation with 'inner' is necessary because of how cpp
 * works.

#define CONCAT_INNER(ns, funcname) ns ## funcname
#define CONCAT(ns, funcname) CONCAT_INNER(ns, funcname)
*/

static const char *BULLETS[] = {"ঈ", "꣐", "𝀒", "٭", "𝄢", "𝄓", "𝄋", "ꢝ"};

/* Caller must not free.
 */
const char *get_bullet () {
  return BULLETS[3];
}

void ac_autoflush (FILE *handle) {
  setvbuf (handle, NULL, _IONBF, 0);
}

bool ac_atod (const char *s, double *ret) {
  char *endptr;
  errno = 0;
  double d = strtod (s, &endptr);
  if (errno || (endptr - s != strlen (s)))
    return false;
  if (!ret) return false;
  *ret = d;
  return true;
}

bool ac_atoi (const char *s, int *ret) {
  char *endptr;
  errno = 0;
  int i = strtol (s, &endptr, 10);
  if (errno || (endptr - s != strlen (s)))
    return false;
  if (!ret) return false;
  *ret = i;
  return true;
}

int ac_int_length (long i) {
  if (i == 0) return 1;
  if (i < 0) return 1 + ac_int_length (-1 * i);
  return 1 + (int) log10 (i);
}

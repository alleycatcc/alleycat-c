// --- `fileno`
#define _GNU_SOURCE
#define _FORTIFY_SOURCE 2

#include <string.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

#include "include/private/mem.h"
#include "include/private/string.h"

#include <stdint.h>

/* Note that enums are static by default.
 */
enum COLORS {
  RED=1,
  BRIGHT_RED,
  GREEN,
  BRIGHT_GREEN,
  YELLOW,
  BRIGHT_YELLOW,
  BLUE,
  BRIGHT_BLUE,
  CYAN,
  BRIGHT_CYAN,
  MAGENTA,
  BRIGHT_MAGENTA,
};

/* Indexed by COLORS enum, except reset.
 */
static char *COL[] = {
  // reset
  "[0m",
  // red
  "[31m",
  // bright red
  "[91m",
  // green
  "[32m",
  // bright green
  "[92m",
  // yellow
  "[33m",
  // bright yellow
  "[93m",
  // blue
  "[34m",
  // bright blue
  "[94m",
  // cyan
  "[36m",
  // bright cyan
  "[96m",
  // magenta
  "[35m",
  // bright magenta
  "[95m",
};

#define set_static_string(fmt) do { \
  ac_spr ("%s", fmt); \
} while (0)

#define MAKE_COLOR(name, enumval) \
  name (const char * const s) { \
    char * const c = _color (s, enumval); \
    set_static_string (c); \
    free (c); \
  }

#define MAKE_M_COLOR(name, enumval) \
  name (const char *s) { \
    return _color (s, enumval); \
  }

static bool disable_colors = false;
static bool force_colors = false;

/* Caller should free the result.
 * In the case of error conditions we return an (almost identical) copy of
 * the input, which must still be freed.
 */
static char *_color (const char *s, int idx) {
  size_t const slen = strlen (s);
  size_t const len = slen + 5 + 4 + 1;
  if (slen == SIZE_MAX) {
    fprintf (stderr, "Warning: _color (): invalid string, truncating 1 char and returning\n");
    char * const copy = ac_malloc (slen * sizeof (char));
    memcpy (copy, s, slen - 1);
    copy[slen - 1] = '\0';
    return copy;
  }
  if (len < slen) {
    fprintf (stderr, "Warning: _color (): size overflow, returning input string\n");
    char * copy = ac_malloc ((slen + 1) * sizeof (char));
    memcpy (copy, s, slen + 1);
    return copy;
  }
  char * const t = ac_malloc (len * sizeof (char));
  bool const disable = !force_colors && (
    disable_colors || !isatty (fileno (stdout))
  );
  const char * const a = disable ? "" : COL[idx];
  const char * const b = disable ? "" : COL[0];
  sprintf (t, "%s%s%s", a, s, b );
  return t;
}

/* The `_m_` versions return a malloc'ed string which the caller should
 * free.
 */
char *MAKE_M_COLOR (ac_m_red, RED)
char *MAKE_M_COLOR (ac_m_bright_red, BRIGHT_RED)
char *MAKE_M_COLOR (ac_m_green, GREEN)
char *MAKE_M_COLOR (ac_m_bright_green, BRIGHT_GREEN)
char *MAKE_M_COLOR (ac_m_yellow, YELLOW)
char *MAKE_M_COLOR (ac_m_bright_yellow, BRIGHT_YELLOW)
char *MAKE_M_COLOR (ac_m_blue, BLUE)
char *MAKE_M_COLOR (ac_m_bright_blue, BRIGHT_BLUE)
char *MAKE_M_COLOR (ac_m_cyan, CYAN)
char *MAKE_M_COLOR (ac_m_bright_cyan, BRIGHT_CYAN)
char *MAKE_M_COLOR (ac_m_magenta, MAGENTA)
char *MAKE_M_COLOR (ac_m_bright_magenta, BRIGHT_MAGENTA)

void MAKE_COLOR (ac_red, RED)
void MAKE_COLOR (ac_bright_red, BRIGHT_RED)
void MAKE_COLOR (ac_green, GREEN)
void MAKE_COLOR (ac_bright_green, BRIGHT_GREEN)
void MAKE_COLOR (ac_yellow, YELLOW)
void MAKE_COLOR (ac_bright_yellow, BRIGHT_YELLOW)
void MAKE_COLOR (ac_blue, BLUE)
void MAKE_COLOR (ac_bright_blue, BRIGHT_BLUE)
void MAKE_COLOR (ac_cyan, CYAN)
void MAKE_COLOR (ac_bright_cyan, BRIGHT_CYAN)
void MAKE_COLOR (ac_magenta, MAGENTA)
void MAKE_COLOR (ac_bright_magenta, BRIGHT_MAGENTA)

void ac_disable_colors () {
  disable_colors = true;
}

void ac_enable_colors () {
  disable_colors = false;
}

void ac_force_colors () {
  force_colors = true;
}

void ac_unforce_colors () {
  force_colors = false;
}

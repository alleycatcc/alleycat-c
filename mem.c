#include <execinfo.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>

#include "include/private/mem.h"

#define BACKTRACE_NUM_FRAMES 20

/* Needs to be compiled with `-rdynamic` to see the symbol names.
 */
void oom (bool fatal) {
  void *trace[BACKTRACE_NUM_FRAMES];
  int size = backtrace (trace, BACKTRACE_NUM_FRAMES);
  fprintf (stderr, "ac: malloc failure (out of memory or memory corruption?)");
  fprintf (stderr, fatal ? ", exiting.\n" : "\n");
  backtrace_symbols_fd (trace, size, 2);
  if (fatal) exit (1);
}

/* `s` of 0 is valid.
 *
 * These are functions, not macros, so that this syntax works:
 *
 *     struct x *x = ac_malloc (sizeof (struct x));
 *
 * We make an attempt to show a trace. __FILE__ and __LINE__ won't work
 * because it's a function, so we use `backtrace`. And `backtrace` doesn't
 * always work (e.g. on RPi).
 *
 * GCC statement expressions would also work, at the cost of portability.
 */

static void *_ac_malloc (size_t s, bool fatal) {
  void * const ptr = malloc (s);
  if (!ptr && s) oom (fatal);
  return ptr;
}

void *ac_malloc_non_fatal (size_t s) {
  return _ac_malloc (s, false);
}

void *ac_malloc (size_t s) {
  return _ac_malloc (s, true);
}

static void *_ac_calloc (size_t nmemb, size_t size, bool fatal) {
  void * const ptr = calloc (nmemb, size);
  if (!ptr && nmemb && size) oom (fatal);
  return ptr;
}

void *ac_calloc_non_fatal (size_t nmemb, size_t size) {
  return _ac_calloc (nmemb, size, false);
}

void *ac_calloc (size_t nmemb, size_t size) {
  return _ac_calloc (nmemb, size, true);
}

static void *_ac_realloc (void *ptr, size_t size, bool fatal) {
  void * const new = realloc (ptr, size);
  if (!new && size) oom (fatal);
  return new;
}

void *ac_realloc_non_fatal (void *ptr, size_t size) {
  return _ac_realloc (ptr, size, false);
}

void *ac_realloc (void *ptr, size_t size) {
  return _ac_realloc (ptr, size, true);
}
